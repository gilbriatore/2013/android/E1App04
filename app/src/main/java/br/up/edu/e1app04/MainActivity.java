package br.up.edu.e1app04;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void converter(View v){

        EditText caixaCelsius = (EditText) findViewById(R.id.txtCelsius);
        EditText caixaFahrenheit = (EditText) findViewById(R.id.txtFahrenheit);

        String txtCelsius = caixaCelsius.getText().toString();

        double vlrCelsius = Double.parseDouble(txtCelsius);
        double fahrenheit = (9 * vlrCelsius + 160) / 5;

        caixaFahrenheit.setText(String.valueOf(fahrenheit));

    }

}







